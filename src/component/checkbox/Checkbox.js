import React from "react";
import "./Checkbox.css";

export class Checkbox extends React.Component {
    render () {
        const {checked, text, onChange} = this.props;

        return (
            <div className="checkbox-inline" onClick={() => onChange && onChange(!checked)}>
                <div className="checkbox">
                    {checked && (<i className="material-icons">check</i>)}
                </div>
                <div className="text">
                    {text}
                </div>
            </div>
        )
    }
}
