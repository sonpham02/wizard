import React from "react";
import "./Select.css";

export class Select extends React.Component {
    render () {
        let {label, className, value, onChange} = this.props;

        return (
            <div className={`material-select ${className}`}>
                <select value={value} onChange={onChange}>
                    <option></option>
                    <option value="saab">Saab</option>
                    <option value="opel">Opel</option>
                    <option value="audi">Audi</option>
                </select>

                <label className={(value && value.length > 0) && "has-value"}>{label}</label>
            </div>
        )
    }
}
