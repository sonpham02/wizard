import React from "react";
import "./TextArea.css";

export class TextArea extends React.Component {
    render () {
        const {label} = this.props;
        return (
            <div className="textarea">
                <div className="label">{label}</div>
                <textarea rows="6"/>
            </div>
        )
    }
}
