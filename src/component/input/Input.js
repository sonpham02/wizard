import React, {Component} from "react";
import "./Input.css";

export class Input extends Component {
    render () {
        let {label, className, value} = this.props;

        return (
            <div className={`material-input ${className}`}>
                <input
                    {...this.props}
                />
                <label className={(value && value.length > 0) && "has-value"}>{label}</label>
            </div>
        );
    }
}
