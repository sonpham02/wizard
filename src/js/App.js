import React, {Component} from 'react';
import wizardBook from '../assets/images/wizard-book.jpg';
import './App.css';
import {WizardContainer} from "./wizard/WizardContainer";

export class App extends Component {
    render() {
        return (
            <div className="app-container" style={{backgroundImage: `url(${wizardBook})`, height: 'auto'}}>
                <WizardContainer/>
            </div>
        );
    }
}

