import React from "react";
import "./Wizard.css"
import {WizardSteps} from "./steps/WizardSteps";
import {AccountStep, StringUtils, validateStep1} from "./steps/account/AccountStep";
import {RoomStep} from "./steps/room/RoomStep";
import {ExtraStep} from "./steps/extra/ExtraStep";

export class Wizard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {}
        }
    }

    steps = [
        {
            id: 0,
            title: "Account",
            render: (props) =>
                <AccountStep
                    {...props}
                    data={this.state.data}
                />,
            onChangeStep: (comp) => this.setState({data: {...this.state.data, ...comp.getData()}}),
            isDone: () => validateStep1(this.state.data)
        },
        {
            id: 1,
            title: "Room Type",
            render: (props) =>
                <RoomStep
                    {...props}
                    data={this.state.data}
                />,
            onChangeStep: (comp) => this.setState({data: {...this.state.data, ...comp.getData()}}),
            isDone: () => !!this.state.data.selectedItem
        },
        {
            id: 2,
            title: "Extra Details",
            render: (props) =>
                <ExtraStep
                    {...props}
                    data={this.state.data}
                />,
            onChangeStep: (comp) => this.setState({data: {...this.state.data, ...comp.getData()}}),
            isDone: null
        }
    ];

    render () {
        return (
            <div className="wizard">
                <div className="header">
                    <div className="header-title">
                        Book a Room
                    </div>
                    <div className="header-desc">
                        This information will let us know more about you.
                    </div>
                </div>

                <WizardSteps steps={this.steps}/>
            </div>
        )
    }
}
