import React from "react";
import {Wizard} from "./Wizard";
import "./WizardContainer.css"

export class WizardContainer extends React.Component {
    render () {
        return (
            <div className="wizard-container">
                <Wizard/>
            </div>
        )
    }
}
