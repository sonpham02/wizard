import React from "react";
import "./ExtraStep.css";
import {TextArea} from "../../../../component/textarea/TextArea";

export class ExtraStep extends React.Component {
    getData = () => {
        return {};
    };

    render () {

        const {formInfo} = this.props;
        formInfo && formInfo({valid: true});

        return (
            <div className="extra-step">
                <div className="extra-header">
                    Drop us a small description.
                </div>

                <div className="extra-content">
                    <TextArea label={"Room description"}/>

                    <div className="sample">
                        <div>
                            Example
                        </div>
                        "The room really nice name is recognized as being a really awesome room. We use it every sunday when we go fishing and we catch a lot. It has some kind of magic shield around it."
                    </div>
                </div>

            </div>
        )
    }
}
