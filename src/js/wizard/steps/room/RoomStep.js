import React from "react";
import "./RoomStep.css";

export class RoomStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeItem: props.data.selectedItem || null
        }
    }

    list = [
        { icon: "weekend", title: "Single"},
        { icon: "home", title: "Family"},
        { icon: "business", title: "Business"},
    ];

    getData = () => {
        return {selectedItem: this.state.activeItem}
    };

    render () {
        const {activeItem} = this.state;
        const {formInfo} = this.props;

        formInfo && formInfo({valid: !!this.state.activeItem});

        return (
            <div className="room-step">
                <div className="room-header">
                    What type of room would you want?
                </div>

                <div className="list">
                    {this.list.map((item, key) => (
                        <div
                            className={`item`}
                            key={key}
                            onClick={() => this.setState({activeItem: item})}
                        >
                            <div className={`icon ${(activeItem && this.list.findIndex((t) => t.icon === activeItem.icon) === key) ? "active" : ""}`}>
                                <i className="material-icons">{item.icon}</i>
                            </div>
                            <div className="title">
                                {item.title}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}
