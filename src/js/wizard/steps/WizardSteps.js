import React from "react";
import "./WizardSteps.css";
import {Functionize} from "../../../component/Functionize";

export class WizardSteps extends React.Component {
    currentForm = null;

    constructor(props) {
        super(props);
        this.state= {
            activeStep: props.steps[0],
            formInfo: null,
            attemptedSubmit: false
        }
    }

    isDone = (step) => {
        return step.isDone == null || step.isDone()
    };

    isEnabled(step) {
        let stepIndex = this.props.steps.findIndex((s) => s.id == step.id);

        for (let i = 0; i < stepIndex; i++) {
            let prevStep = this.props.steps[i];
            if (!this.isDone(prevStep)) {
                return false;
            }
        }
        return true;
    }

    goNext = () => {
        const { activeStep } = this.state;
        const {steps} = this.props;
        activeStep.onChangeStep(this.currentForm);

        let stepIndex = steps.findIndex((s) => s.id == activeStep.id);

        if(stepIndex != steps.length - 1) {
            this.setState({activeStep: steps[stepIndex + 1]})
        }
    };

    render () {
        const {activeStep, formInfo, attemptedSubmit} = this.state;
        const {steps} = this.props;

        return (
            <div className="wizard-steps">
                <div className="navigation">
                    {steps.map((step, i) => (
                        <div
                            className="nav-item" style={{width: `calc(100% / ${steps.length})`}}
                            key={i}
                            onClick={() => {
                                if(activeStep.id != step.id && this.isEnabled(step)) {
                                    activeStep.onChangeStep(this.currentForm);
                                    this.setState({activeStep: step});
                                }
                            }}
                        >
                            {step.title}
                        </div>
                    ))}

                    {(() => {
                        let width = 750 / steps.length;
                        let stepIndex = steps.findIndex((s) => s.id == activeStep.id);
                        let padding = stepIndex === 0 ? -8 : (stepIndex === steps.length - 1 ? 8 : 0);

                        return (
                            <div className="moving-active-tab"
                                 style={{
                                     width: `${width}px`,
                                     transform: `translate3d(${`${(width * stepIndex) + padding}px`}, 0px, 0px)`,
                                     transition: `all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)`
                                 }}
                            >
                                {activeStep.title}
                            </div>
                        )
                    })()}


                </div>

                <div className="content">
                    {React.cloneElement(
                        activeStep.render({
                            formInfo,
                            attemptedSubmit
                        }), {
                            ref: (form) => this.currentForm = form,
                        }
                    )}
                </div>

                <Functionize
                    refState={(formInfo) => this.setState({formInfo})}
                    render={({valid}) => {
                        let stepIndex = steps.findIndex((s) => s.id == activeStep.id);
                        return (
                            <div className="footer">
                                { stepIndex != 0 ? (
                                    <button className="btn btn-default" onClick={() => this.setState({activeStep: steps[stepIndex - 1]})}>
                                        Previous
                                    </button>
                                ) :
                                    <div/>
                                }

                                <button className={`btn ${valid ? "btn-danger" : "btn-default"}`} onClick={() => valid ? this.goNext() : this.setState({attemptedSubmit: true})}>
                                    {(stepIndex == steps.length - 1) ? "Finish" : "Next"}
                                </button>
                            </div>
                        )
                    }}
                />
            </div>
        )
    }
}
