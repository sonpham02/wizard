import React, {Fragment} from "react";
import {Input} from "../../../../component/input/Input";
import "./AccountStep.css";
import {Select} from "../../../../component/select/Select";
import {Checkbox} from "../../../../component/checkbox/Checkbox";

export const StringUtils = {
    isEmpty(val) {
        return val == null || val === "";
    },
    isBlank(val) {
        return this.isEmpty(val) || val.replace(/\s/g, "").length == 0;
    }
};

let invalidFields = (data) => {
    let validators = {
        email: (value) => !StringUtils.isBlank(value),
        password: (value) => !StringUtils.isBlank(value),
        country: (value) => !StringUtils.isBlank(value),
        budget: (value) => !StringUtils.isBlank(value),
    };


    let invalidFields = [];

    Object.keys(validators).forEach((key) => {
        let valid = validators[key];
        if( !valid(data[key]) ) {
            invalidFields.push(key)
        }
    });

    return invalidFields;
}

export let validateStep1 = (data) => {
    return invalidFields(data).length == 0;
};

export class AccountStep extends React.Component {
    constructor(props) {
        super(props);

        let getInitData = (data) => {
            return {
                email: data.email || "",
                password: data.password || "",
                country: data.country || "",
                budget: data.budget || "",
                checked: data.checked || false
            }
        };

        this.state = {
            ...getInitData(props.data)
        }
    }

    getData = () => {
        return this.state;
    };

    render () {
        const { email, password, country, budget, checked } = this.state;
        const {formInfo, attemptedSubmit} = this.props;

        formInfo && formInfo({valid: validateStep1(this.state)});

        let invalidFs = invalidFields(this.state);

        return (
            <Fragment>
                <div className="account-step">
                    <div className="account-title">
                        Let's start with the basic details.
                    </div>
                    <div className="account-form">
                        <div className="account-form-group">
                            <div className="input-group">
                                <i className="material-icons">email</i>
                                <Input label="Your Email"
                                       className={`${attemptedSubmit && invalidFs.indexOf("email") > -1 ? "error" : "" }`}
                                       value={email} onChange={(e) => this.setState({email: e.target.value})}/>
                            </div>

                            <div className="input-group">
                                <i className="material-icons">lock</i>
                                <Input label="Your Password"
                                       type="password"
                                       value={password} onChange={(e) => this.setState({password: e.target.value})}
                                       className={`${attemptedSubmit && invalidFs.indexOf("password") > -1 ? "error" : "" }`}
                                />
                            </div>
                        </div>

                        <div className="account-form-group">
                            <Select
                                className={`${attemptedSubmit && invalidFs.indexOf("country") > -1 ? "error" : "" }`}
                                label="Country" value={country} onChange={(e) => this.setState({country: e.target.value})}/>
                            <Select
                                className={`${attemptedSubmit && invalidFs.indexOf("budget") > -1 ? "error" : "" }`}
                                label="Daily Budget" value={budget} onChange={(e) => this.setState({budget: e.target.value})}/>
                        </div>
                    </div>
                </div>

                <div className="subscribe">
                    <Checkbox
                        text={"Subscribe to our newsletter"}
                        checked={checked}
                        onChange={(checked) => this.setState({checked})}
                    />
                </div>
            </Fragment>

        )
    }
}
